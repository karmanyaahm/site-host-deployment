#!/bin/bash
PATH="/usr/local/bin:/usr/bin"

mkdir -p $HOME/store
date
for i in {{IPFS_PINS_LIST | join(" ")}}
do
	echo $i
	DATE=$(date +%F_%T)
	ipfs files mkdir -p /$i
	LOC=$(ipfs resolve -r /ipns/$i) || continue
	echo $LOC
	if ipfs files ls -l /$i | tail -n1 | grep -q ${LOC#/ipfs/}; then
		echo already downloaded
		continue
	else
		echo copying
   	        ipfs files cp "$LOC" /${i}/$DATE
	fi
  	ipfs refs -r $LOC > /dev/null

	ipfs get -o $HOME/store/$i $LOC && \
	  rsync -a --delete $HOME/store/$i/ /srv/$i/
done

rm -rf $HOME/store
ipfs pin add {% for i in STATIC_CIDS %} /ipfs/{{i}} {% endfor %}


date
